from django.urls import path
from accounts.views import login_form, logout_user, signup_form


urlpatterns = [
    path("login/", login_form, name="login"),
    path("logout/", logout_user, name="logout"),
    path("signup/", signup_form, name="signup"),
]
