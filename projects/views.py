from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProject


# Create your views here.
@login_required
def list_projects(request):
    lst = Project.objects.filter(owner=request.user)
    context = {"lst": lst}
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    details = Project.objects.get(id=id)
    context = {
        "details": details,
    }
    return render(request, "projects/details.html", context)


def create_project(request):
    if request.method == "POST":
        form = CreateProject(request.POST)
        if form.is_valid:
            form.save()
            return redirect("list_projects")
    else:
        form = CreateProject()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
